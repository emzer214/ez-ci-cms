/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.35-1ubuntu1 : Database - ci_cms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ci_cms` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ci_cms`;

/*Table structure for table `cms_menus` */

CREATE TABLE `cms_menus` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `type` enum('GROUP','ITEM') NOT NULL DEFAULT 'ITEM',
  `link_type` enum('PAGE','EXTERNAL_LINK') NOT NULL DEFAULT 'PAGE',
  `link_value` text,
  `parent` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `cms_menus` */

insert  into `cms_menus`(`id`,`label`,`type`,`link_type`,`link_value`,`parent`) values (1,'HEADER_SUB','GROUP','PAGE',NULL,0);
insert  into `cms_menus`(`id`,`label`,`type`,`link_type`,`link_value`,`parent`) values (2,'Notifications','ITEM','PAGE','7',1);
insert  into `cms_menus`(`id`,`label`,`type`,`link_type`,`link_value`,`parent`) values (3,'Profile','ITEM','PAGE','8',1);
insert  into `cms_menus`(`id`,`label`,`type`,`link_type`,`link_value`,`parent`) values (4,'Logout','ITEM','EXTERNAL_LINK','/cms/login',1);

/*Table structure for table `cms_pages` */

CREATE TABLE `cms_pages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `page` varchar(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `template` varchar(100) DEFAULT NULL,
  `type` enum('template','ajax') NOT NULL DEFAULT 'template',
  `show_header` tinyint(1) NOT NULL DEFAULT '1',
  `show_footer` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `cms_pages` */

insert  into `cms_pages`(`id`,`page`,`title`,`template`,`type`,`show_header`,`show_footer`) values (1,'dashboard','CMS Dashboard',NULL,'template',1,1);
insert  into `cms_pages`(`id`,`page`,`title`,`template`,`type`,`show_header`,`show_footer`) values (2,'404','Page Not Found',NULL,'template',1,1);
insert  into `cms_pages`(`id`,`page`,`title`,`template`,`type`,`show_header`,`show_footer`) values (3,'login','CMS Login',NULL,'template',0,0);
insert  into `cms_pages`(`id`,`page`,`title`,`template`,`type`,`show_header`,`show_footer`) values (4,'login/authenticate',NULL,NULL,'ajax',1,1);
insert  into `cms_pages`(`id`,`page`,`title`,`template`,`type`,`show_header`,`show_footer`) values (5,'pages','Pages','datalist','template',1,1);
insert  into `cms_pages`(`id`,`page`,`title`,`template`,`type`,`show_header`,`show_footer`) values (6,'pages/data','Pages Data',NULL,'template',1,1);
insert  into `cms_pages`(`id`,`page`,`title`,`template`,`type`,`show_header`,`show_footer`) values (7,'notifications','Notifications','datalist','template',1,1);
insert  into `cms_pages`(`id`,`page`,`title`,`template`,`type`,`show_header`,`show_footer`) values (8,'profile','Profile',NULL,'template',1,1);

/*Table structure for table `cms_styles` */

CREATE TABLE `cms_styles` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `style` varchar(100) NOT NULL,
  `page` varchar(100) DEFAULT NULL,
  `sequence` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `cms_styles` */

insert  into `cms_styles`(`id`,`style`,`page`,`sequence`) values (1,'style','',10);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
