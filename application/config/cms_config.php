<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config['default_controller']='dashboard';
$config['db_group']='default';
$config['cms_segment']='cms';
$config['cms_style_path']='public/css/cms/';
$config['cms_login_style']='login';