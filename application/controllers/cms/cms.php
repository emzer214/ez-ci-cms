<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller {

	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function _remap()
	{
		$this->load->helper("form");
		$this->load->helper("url");
		$this->load->helper("cms/form");
		$this->load->helper("cms/css");
		$this->load->helper("cms/menu");
		
		
		$this->load->model('cms/pages');
		$page = $this->getSegments();
		
		
		$query=$this->pages->getPageData($page);
		if($query->num_rows())
		{
			$data["page"] = $query->row();
		}
		else
		{
			$query=$this->pages->getPageData("404");
			$data["page"] = $query->row();
		}
		
		if($data["page"]->type!="ajax" && $data["page"]->show_header == 1)
		{
			$this->_header($page);
		}
		
		
		$file_name =  $this->uri->segment(2);
		$this->config->load('cms_config');
        $file_path = APPPATH.'controllers/cms/'.$file_name.'.php';
		if(file_exists($file_path)){
			require $file_path;
        	$object_name = $file_name;
        	$class_name = ucfirst($file_name);
			$controller = ucfirst($file_name);
			$this->$object_name = new $class_name();
			if($this->uri->segment(3)>"")
			{
				$method = $this->uri->segment(3);
				if(method_exists($this->$object_name,$method ))
				{
					$this->$object_name->$method();
				}
				else
				{
					$this->_404();
				}
			}
			else
			{
				if(method_exists($this->$object_name,"index"))
				{
					$this->$object_name->index();
				}
				else
				{
					$this->_404();
				}
			}
		}
		else
		{
			
			if($page!="")
			{
				$query=$this->pages->getPageData($page);
				if($query->num_rows())
				{
					$data["page"] = $query->row();
				}
				else
				{
					$query=$this->pages->getPageData("404");
					$data["page"] = $query->row();
				}
				
				
				if($data["page"]->template!="")
				{
					$this->load->view("cms/templates/".$data["page"]->template,$data);
				}
				else
				{
					$this->load->view("cms/templates/404",$data);
				}
			
			}
			else
			{
        		$file_path = APPPATH.'controllers/cms/dashboard.php';
				require $file_path;
				$this->dashboard = new Dashboard();
				$this->dashboard->index();
			}
		}
		if($data["page"]->type!="ajax" && $data["page"]->show_header == 1)
		{
			$this->_footer($page);
		}
		
	}
	
	public function getSegments()
	{
		$this->load->helper("url");
		$page = "";
		
		for($i=2;$i<100;$i++)
		{
			if($this->uri->segment($i)>"")
			{				
				if(strlen($page)>1)
				{
					$page .= "/";
				}
				$page .= $this->uri->segment($i);
			}
			else
			{
				break;
			}
		}
		return $page;
	}
	public function _header($page)
	{
		$this->load->model('cms/pages');
		$page = "dashboard";
		
		
		$query=$this->pages->getPageData($page);
		if($query->num_rows())
		{
			
			$data["page"] = $query->row();
		}
		else
		{
			$query=$this->pages->getPageData("404");
			$data["page"] = $query->row();
		}
			$this->load->view("cms/header",$data);
	}
	public function _footer($page)
	{
		$this->load->view("cms/footer");
	}
	public function _404()
	{
			echo "Not Found";
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */