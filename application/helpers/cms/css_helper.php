<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$CI =& get_instance(); 
$CI->config->load('cms_config');

function cms_style($page='')
{
	global $CI;
	$styles = "";
	if($page=="login")
	{
		$styles .=  "<link rel=\"stylesheet\" type=\"text/css\" href=\"/".rtrim($CI->config->item("cms_style_path"),'/')."/".rtrim($CI->config->item("cms_login_style"),'/').".css\" />";
	}
	else
	{
		$CI->load->model("cms/styles");
		$query = $CI->styles->getPageStyles($page);
		$styleData = $query->result();
		foreach($styleData as $style)
		{
			$styles .=  "<link rel=\"stylesheet\" type=\"text/css\" href=\"/".rtrim($CI->config->item("cms_style_path"),'/')."/".rtrim($style->style,'.css').".css\" />";
		}
	}
	return $styles;
}