<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$CI =& get_instance(); 
$CI->config->load('cms_config');
		
function cms_form_open($link,$attributes = array())
{
	global $CI;
	return form_open($CI->config->item("cms_segment")."/".$link,$attributes);
}

		
function cms_form_open_multipart($link,$attributes = array())
{
	global $CI;
	return form_open_multipart($CI->config->item("cms_segment")."/".$link,$attributes);
}