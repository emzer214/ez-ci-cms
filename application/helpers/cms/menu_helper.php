<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$CI =& get_instance(); 
$CI->config->load('cms_config');

function cms_menus($group='')
{
	global $CI;
		$CI->load->model("cms/menus");
		$query = $CI->menus->getByGroup($group);
	return $query->result();
}