<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page->title; ?></title>
<?php echo cms_style($page->page); ?>
</head>

<body>
<div class="main-container">
	<div class="header">
    	<h1 class="branding">CMS Testing</h1>
        <ul class="account-group">
        <?php foreach(cms_menus('HEADER_SUB') as $menu): ?>
        	<li><a href="<?=$menu->link?>"><?=$menu->label?></a></li>
        <?php endforeach; ?>
        </ul>
    </div>
    <div class="content">
    