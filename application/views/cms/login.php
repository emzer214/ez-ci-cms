<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CMS Login</title>
	<?php echo cms_style('login'); ?>
</head>

<body>
<div class="container">
	<div class="login-content">
    <h1> CMS Login </h1>
	<?php echo cms_form_open('login/authenticate',array("class"=>"ajax login-form")); ?>
    <input name="username" type="text" />
    <input name="password" type="password" />
    <input name="Login" type="submit" value="Login" id="login-button" />
    </div>
</div>
</body>
</html>
