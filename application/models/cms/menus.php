<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menus extends CI_Model {
	
	function getByGroup($group)
	{
		$CI =& get_instance(); 
		$CI->load->model('cms/crud');
		
		$sql="	SELECT  cms_menus.id,
				cms_menus.`label`,
				cms_menus.`link_type`,
				IF(cms_menus.`link_type`='EXTERNAL_LINK',cms_menus.`link_value`,(SELECT page FROM cms_pages WHERE cms_pages.id = cms_menus.`link_value`)) AS link,
				cms_menus.`parent`,
				cms_menus.`type`
				FROM cms_menus 
				INNER JOIN cms_menus parent ON parent.id = cms_menus.parent
				WHERE parent.label = ? AND cms_menus.`type` = 'ITEM'";
		$query = $CI->crud->query($sql,$group);
		return $query;
	}
	
}