<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Styles extends CI_Model {
	
	function getPageStyles($page)
	{
		$this->load->model('cms/crud');
		if($page != "login")
		{
			$sql="select * from cms_styles where page = ? OR page = '' order by sequence ASC";
			return $this->crud->query($sql,$page);
		}
	}
	
}