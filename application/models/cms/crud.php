<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud extends CI_Model {
        private $DB;
        public $last_insert_id, $connection_group, $num_rows, $num_pages, $total_rows, $last_query;
        
        
        public function getAvailableFields($table, $data, $noKey = false)
        {
                $temp = array();
                $sql = "DESCRIBE {$table}";
                $query = $this->DB->query($sql);
                foreach($query->result() as $row)
                {
                        if($noKey)
                        {
                                if(in_array($row->Field,$data))
                                {
                                        $temp[] = $row->Field;
                                }
                        }
                        else
                        {
                                if(array_key_exists($row->Field,$data))
                                {
                                        $temp["`".$row->Field."`"] = $data[$row->Field];
                                }
                        }
                }
                return $temp;
        }
        public function arrayKeysToSet($array,$isWhere = false)
        {
                
                $glue = " = ? ";
                if($isWhere)
                {       
                        $glue = " AND ";
                        foreach(array_keys($array) as $field)
                        {
                                if(!empty($field))
                                {
                                        $fields[]=$field . " = ? ";
                                }
                        }
                        $temp = implode($glue,$fields); 
                }
                else
                {
                        foreach(array_keys($array) as $field)
                        {
                                if(!empty($field))
                                {
                                        $fields[]=$field . " = ? ";
                                }
                        }
                        $temp = implode(", ",$fields);  
                }
                return $temp;
        }
        
        public function connect($group = 'default')
        {
                $this->connection_group = $group;
                $this->DB=$this->load->database($group,true);
        }
        
        
        public function query($sql, $array = array(), $group = NULL)
        {
			if($group == NULL)
			{
                $this->connect();
			}
			else
			{
				$this->connect($group);
			}
			$query = $this->DB->query($sql,$array);
			$this->last_query = $this->DB->last_query();
			$this->DB->close();
			
			return $query;
                
        }
        public function select($table,$columns = NULL,$where = NULL,$group = NULL,$order = NULL,$limit = NULL,$page = NULL)
        {
                if(!isset($this->DB))
                {
                        $this->connect();
                }
                if(isset($columns))
                {
                        if(is_array($columns))
                        {
                                $field = $this->getAvailableFields($table,$columns,true);
                                if(count($field)>0)
                                {
                                        $columns = implode(', ',$field);        
                                }
                                else
                                {
                                        $columns = " * ";
                                }
                        }
                }
                else
                {
                        $columns = " * ";
                }
                $sql = "SELECT " . $columns . " FROM `" . $table. "`";
                http://passwordmanager.dev/pmanage/index.php/users/add
                $fields=array();
                if(isset($where))
                {
                        if(is_array($where))
                        {
                                $field = $this->getAvailableFields($table,$where);
                                $sql .= " WHERE ".$this->arrayKeysToSet($field,true);
                                $fields = array_merge($fields,$field);
                        }
                        else
                        {
                                $sql .= " WHERE ".$where;
                        }
                }
                
                if(isset($group))
                {
                        if(is_array($group))
                        {
                                $field = $this->getAvailableFields($table,$group,true);
                                if(count($field)>0)
                                {
                                        $sql .= " GROUP BY ".implode(', ',$field);
                                }
                        }
                        else
                        {
                                $sql .= " GROUP BY ".$group;
                        }
                }
                
                if(isset($order))
                {
                        if(is_array($order))
                        {
                                $field = $this->getAvailableFields($table,$order, true);
                                if(count($field)>0)
                                {
                                        $sql .= " ORDER BY ".implode(', ',$field);
                                }
                        }
                        else
                        {
                                $sql .= " ORDER BY ".$order;
                        }
                }
                $query = $this->DB->query($sql,$fields);
				$this->total_rows = $query->num_rows();
				$this->num_pages = (isset($page)?ceil($this->total_rows/$limit):'1');
                if(isset($limit) && !isset($page))
                {
                        $sql .= " LIMIT ".$limit;
                }
                
                if(isset($limit) && isset($page))
                {
                  $from = ($page*$limit)-$limit;
          
                        $sql .= " LIMIT ".($from>=0?$from:0).",".($from>=0?$limit:0);
                }
                
                error_reporting(E_ALL & ~E_DEPRECATED);
                $query = $this->DB->query($sql,$fields);
                error_reporting(E_ALL);
                $this->num_rows = $query->num_rows();
                $this->DB->close();
                
    			return $query;
        }
        public function insert($table,$data)
        {
                
        		if(!array_key_exists("encode_date", $data))
        		{
        			$data["encode_date"]=date("Y-m-d H:i:s");
        		}
                
        		$data["encode_id"]=$this->session->userdata("user_id");
        		$data["update_id"]=$this->session->userdata("user_id");;
    			if(!isset($this->DB))
                {
                        $this->connect();
                }
                $fields = $this->getAvailableFields($table,$data);
                if(count($fields)>0)
                {
                        $set = $this->arrayKeysToSet($fields);
                        $sql = "INSERT INTO `" . $table . "` SET " . $set ;
                        
                        $this->DB->query($sql,$fields);
                        $this->last_insert_id = $this->DB->insert_id();
                }
                $this->DB->close();
        }
        public function update($table,$data,$set_rec_id,$get_rec_id)
        {
                if(!isset($this->DB))
                {
                        $this->connect();
                }
        		$data["update_id"]=$this->session->userdata("user_id");;
                $fields = $this->getAvailableFields($table,$data);
                if(count($fields)>0)
                {
                        $set = $this->arrayKeysToSet($fields);
                        $sql = "UPDATE `" . $table . "` SET " . $set ;
                        if(is_array($get_rec_id))
                        {
                                $sql .= " WHERE ".$this->arrayKeysToSet($where);
                                $fields = array_merge($fields,$where);
                        }
                        else
                        {
                                $sql .= " WHERE ".$set_rec_id." = ".$get_rec_id;
                        }
      
                        error_reporting(E_ALL & ~E_DEPRECATED);
                        $this->DB->query($sql,$fields);
                        error_reporting(E_ALL);
                }
                $this->DB->close();
        }
		
        public function delete($table,$where)
        {
                if(!isset($this->DB))
                {
                        $this->connect();
                }
                $sql = "DELETE FROM `" . $table . "`";
                if(is_array($where))
                {
                        $sql .= " WHERE ".$this->arrayKeysToSet($where);
                        $fields = $where;
                }
                else
                {
                        $sql .= " WHERE ".$where;
                }
                
                error_reporting(E_ALL & ~E_DEPRECATED);
                $this->DB->query($sql,$fields);
                error_reporting(E_ALL);
                $this->DB->close();             
        }
        
        
}