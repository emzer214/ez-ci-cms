<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Model {
	
	function getPageData($page)
	{
		$CI =& get_instance(); 
		$CI->load->model('cms/crud');
		
		$sql="select * from cms_pages where page = ? ";
		$query = $CI->crud->query($sql,$page);
		return $query;
	}
	
}